- [x] proton-get check for a newer version of itself (???);
- [x] crontab job and notification with notify-send (???);
- [ ] check space ocupied by the current installed one(s). Account for BTRFS(??);
- [x] implement proton-get --update to update without the need of git cloning;
- [x] notify-send notification for systems with it installed;
- [x] separate the functions that list remote version available and local installed ones;
- [ ] implement list of available releases to choose installation of specific one;
- [x] implement specific version downloading: proton-get -g/--get 0.0-GE-0;
- [ ] add zenity dialogs (???). portable version of zenity ()??);
- [ ] proton-get autoupdate (?????);
- [x] check sum after downloading the file (!!!?);
- [x] update and check for xidel version. Despite 0.9.9 being under development, it is the dev's recommended version;
- [x] account for the new Proton GE files and folder nomenclature and versioning. Now GE-Proton-0-0 instead of Proton-GE-0.0-0;
- [x] also show the url for the release information page;
- [ ] function to show infomation about available releases, like proton-get --info https://github.com/GloriousEggroll/proton-ge-custom/releases/tag/GE-Proton7-37;
- [ ] add --spider to wget using a dryrun variable for the -n --dry-run parameter;
